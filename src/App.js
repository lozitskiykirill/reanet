import React from 'react';
// import logo from './logo.svg';
import Header from "./components/Header/Header";
import Navbar from "./components/Navbar/Navbar";
import Profile from "./components/Profile/Profile";
import Dialogs from "./components/Dialogs/Dialogs";
import './App.css';
import Route from "react-router-dom/Route";
import BrowserRouter from "react-router-dom/BrowserRouter";
import {addMessage, updatedNewMessageText} from "./redux/state";

function App(props) {

  return (
      <BrowserRouter>
        <div className="app-wrapper">
            <Header />
            <Navbar />
            <div className="app-wrapper-content">
                <Route path="/profile"
                       render={ () => <Profile
                           state={props.state.profilePage}
                           dispatch={props.dispatch}
                       />}
                />
                <Route path="/dialogs"
                       render={ () =>
                           <Dialogs
                               state={props.state.messagesPage}
                               dispatch={props.dispatch}
                           />}
                />
            </div>
        </div>
      </BrowserRouter>
  )
}

export default App;
