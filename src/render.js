import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import {addPost} from './redux/state';
import {updatedNewPostText} from "./redux/state";
import {updatedNewMessageText} from "./redux/state";
import  {addMessage} from "./redux/state";

export let reRenderEntireTree = (state) => {
	ReactDOM.render(
	  <React.StrictMode>
	    <App
			state={state}
			addPost={addPost}
			updatedNewPostText={updatedNewPostText}
			addMessage={addMessage}
			updatedNewMessageText = {updatedNewMessageText}
		/>
	  </React.StrictMode>,
	  document.getElementById('root')
	);
}