import React from 'react';
import classes from './Header.module.css';

function Header() {
  return (
      <header className={classes.header}>
        <img src="https://upload.wikimedia.org/wikipedia/commons/3/3e/Cargomatic_%28Company%29_Logo.png" />
      </header>
  )
}

export default Header;
