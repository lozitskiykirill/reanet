import React from 'react';
import classes from './Navbar.module.css';
import NavLink from "react-router-dom/NavLink";

function Navbar() {
  return (
      <nav className={classes.nav}>
        <ul>
            <li className={classes.link}>
                <NavLink to="/profile"  activeClassName={classes.active}>Prrofile</NavLink>
            </li>
            <li className={classes.link}>
                <NavLink to="/dialogs" activeClassName={classes.active}>Messages</NavLink>
            </li>
            <li className={classes.link}>
                <NavLink to="/music" activeClassName={classes.active}>Music</NavLink>
            </li>
            <li className={classes.link}>
                <NavLink to="/settings" activeClassName={classes.active}>Settings</NavLink>
            </li>
        </ul>
      </nav>
  )
}

export default Navbar;
