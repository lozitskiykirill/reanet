import React from 'react';
import classes from './MyPosts.module.css';
import Post from "./Post/Post";
import AddPost from "./AddPost/AddPost.js";

function MyPosts(props) {

    let postsElements = props.post.postData.map(postSingle => <Post message={postSingle.message} likeAmount={postSingle.likeAmount}/>)

    return (
      <div className={classes.myPost}>
        <h2>
          My Posts
        </h2>
        <AddPost
            addPostData={props.post.addPostData}
            dispatch={props.dispatch}
        />
        {postsElements}
    </div>
  )
}

export default MyPosts;