import React from 'react';
import classes from './AddPost.module.css';
import {updateNewPostTextActionCreator} from '../../../../redux/state';
import {addPostActionCreator} from '../../../../redux/state';

function AddPost(props) {
	let newPostElement = React.createRef();

	let addPost = () => {
    props.dispatch(addPostActionCreator());
	};

  let handleKeyPress = (event) => {
    if(event.key === 'Enter'){
      addPost();
    }
  };

  let onPostChange = () => {
	  let text = newPostElement.current.value;
    let action = updateNewPostTextActionCreator(text);
    props.dispatch(action);
  };

    return (
      <div className={classes.addPost}>
      	<h3>add post</h3>
      	<form>
      		<textarea ref={newPostElement} onKeyPress={handleKeyPress} placeholder={props.addPostData.placeholder} value={props.addPostData.newPostText} onChange={onPostChange}/>
      		<div className={classes.button} onClick={ addPost }>add</div>
      	</form>
      </div>
  )
}

export default AddPost;