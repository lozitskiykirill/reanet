import React from 'react';
import classes from './FormPost.module.css';

function FormPost(props) {
    return (
      	<form>
      		<input type="text" placeholder="enter the text"/>
      		<button className={classes.button} type="submit">add</button>
      	</form>
  )
}

export default FormPost;