import React from 'react';
import classes from './Post.module.css';

function Post(props) {

  return (
      <div className={classes.post}>
        <p>{props.message}</p>
        <div className={classes.like}>
            <img src="https://image.flaticon.com/icons/svg/1077/1077035.svg" />
            <span>{props.likeAmount}</span>
        </div>
    </div>
  )
}

export default Post;
