import React from 'react';
import classes from './Message.module.css';

function Message(props) {

    let messageElement = props.messageData.map(messageObject => <div>{messageObject.message}</div>);

    return (
        <div className={classes.message}>
            {messageElement}
        </div>
    )
};

export default Message;
