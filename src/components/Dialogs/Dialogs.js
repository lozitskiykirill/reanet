import React from 'react';
import classes from './Dialogs.module.css'
import {NavLink} from "react-router-dom";
import Message from "./Message/Message";
import AddMessage from "./AddMessage/AddMessage";

function DialogItem(props) {
   let path = "/dialogs/" + props.id;

  return (
      <div className={classes.dialogItem + ' ' + classes.active}>
        <NavLink to={path}>{props.name}</NavLink>
      </div>
    )
}

function Dialogs(props) {

    let dialogElements = props.state.dialogData.map( nameObject => <DialogItem name={nameObject.name} id={nameObject.id}/>)

    return (
        <div className={classes.dialogsWrapper}>
            <div className={classes.dialogList}>
                {dialogElements}
            </div>
            <Message messageData={props.state.messageData}/>
            <AddMessage dispatch={props.dispatch} addMessageData={props.state.addMessageData}/>
        </div>
      )
}

export default Dialogs;
