import React from 'react';
import classes from './AddMessage.module.css';
import {sendMessageActionCreator} from '../../../redux/state';
import {updateNewMessageTextActionCreator} from '../../../redux/state';

function AddMessage(props) {
    let newMessageElement = React.createRef();



    let addMessage = () => {
        props.dispatch(sendMessageActionCreator());
    };

    let handleKeyPress = (event) => {
        if(event.key === 'Enter'){
            addMessage();
        }
    };

    let onMessageChange = () => {
        let text = newMessageElement.current.value;
        let action = updateNewMessageTextActionCreator(text);
        props.dispatch(action);
    };

    return (
        <div className={classes.form}>
            <textarea ref={newMessageElement} onChange={onMessageChange} onKeyPress={handleKeyPress} value={props.addMessageData.newMessageText}/>
            <button type="submit" className={classes.button} onClick={ addMessage }>add</button>
        </div>
    )
}

export default AddMessage;
