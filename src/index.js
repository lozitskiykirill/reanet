import store from "./redux/state";
import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';

let reRenderEntireTree = (state) => {
    ReactDOM.render(
        <React.StrictMode>
            <App
                state={store.getState()}
                dispatch={store.dispatch.bind(store)}
                addMessage={store.addMessage.bind(store)}
                updatedNewMessageText = {store.updatedNewMessageText.bind(store)}
            />
        </React.StrictMode>,
        document.getElementById('root')
    );
};

reRenderEntireTree(store.getState());

store.subscribe(reRenderEntireTree);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();