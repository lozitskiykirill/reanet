const UPDATE_NEW_MESSAGE_TEXT = 'UPDATE_NEW_MESSAGE_TEXT';
const SEND_MESSAGE = 'SEND_MESSAGE';

export const dialogsReducer = (state, action) => {
    if(action.type === SEND_MESSAGE) {
        let newMessage = {
            id: 4,
            message: state.addMessageData.newMessageText,
        };

        state.messageData.push(newMessage);
        state.addMessageData.newMessageText = "";
    }

    else if (action.type === UPDATE_NEW_MESSAGE_TEXT) {
        state.addMessageData.newMessageText = action.updatedMessage;
    }

    return state;
}

export default dialogsReducer;