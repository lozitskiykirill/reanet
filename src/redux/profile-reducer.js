const ADD_POST = 'ADD_POST';
const UPDATE_NEW_POST_TEXT = 'UPDATE_NEW_POST_TEXT';

export const profileReducer = (state, action) => {
    if(action.type === ADD_POST) {
        let newPost = {
            id: 5,
            message: state.addPostData.newPostText,
            likeAmount: 0
        };

        state.postData.push(newPost);
        state.addPostData.newPostText = "";
    }
    else if (action.type === UPDATE_NEW_POST_TEXT) {
        state.addPostData.newPostText = action.updatedText;
    }

    return state;
}

export default profileReducer;