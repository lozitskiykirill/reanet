const ADD_POST = 'ADD_POST';
const UPDATE_NEW_POST_TEXT = 'UPDATE_NEW_POST_TEXT';
const UPDATE_NEW_MESSAGE_TEXT = 'UPDATE_NEW_MESSAGE_TEXT';
const SEND_MESSAGE = 'SEND_MESSAGE';

let store = {
    _state : {
        profilePage: {
            postData: [
                {id: 1, message: "666 test message 1", likeAmount: 13},
                {id: 2, message: "2 message", likeAmount: 5},
                {id: 3, message: "number 3", likeAmount: 5},
                {id: 4, message: "number 4", likeAmount: 8},
            ],
            addPostData: {
                placeholder: 'enter the value',
                newPostText: '',
            },
        },
        messagesPage: {
            dialogData: [
                {id: 1, name: "Sergey"},
                {id: 2, name: "Jack"},
                {id: 3, name: "John Carter"},
                {id: 4, name: "Stacey"},
                {id: 5, name: "Sенене"}
            ],
            messageData: [
                {id: 1, message: "Hello bro 1"},
                {id: 2, message: "Second 2 message"},
                {id: 3, message: "333"},
            ],
            addMessageData: {
                newMessageText: '',
            },
        }
    },
    _reRenderEntireTree() {
        console.log('state has changed');
    },

    getState() {
        return this._state;
    },
    subscribe(observer) {
        this._reRenderEntireTree = observer;
    },

    updatedNewMessageText(updatedText) {
        this._state.messagesPage.addMessageData.newMessageText = updatedText;
        this._reRenderEntireTree(this._state);
    },
    addMessage() {
        let newMessage = {
            id: 4,
            message: this._state.messagesPage.addMessageData.newMessageText
        };
        this._state.messagesPage.messageData.push(newMessage);
        this._state.messagesPage.addMessageData.newMessageText = "";
        this._reRenderEntireTree(this._state);
    },

    dispatch(action) {
        if(action.type === ADD_POST) {
            let newPost = {
                id: 5,
                message: this._state.profilePage.addPostData.newPostText,
                likeAmount: 0
            };

            this._state.profilePage.postData.push(newPost);
            this._state.profilePage.addPostData.newPostText = "";
            this._reRenderEntireTree(this._state);
        }
        else if (action.type === UPDATE_NEW_POST_TEXT) {
            this._state.profilePage.addPostData.newPostText = action.updatedText;
            this._reRenderEntireTree(this._state);
        }
        else if(action.type === SEND_MESSAGE) {
            let newMessage = {
                id: 4,
                message: this._state.messagesPage.addMessageData.newMessageText,
            };

            this._state.messagesPage.messageData.push(newMessage);
            this._state.messagesPage.addMessageData.newMessageText = "";
            this._reRenderEntireTree(this._state);
        }
        else if (action.type === UPDATE_NEW_MESSAGE_TEXT) {
            this._state.messagesPage.addMessageData.newMessageText = action.updatedMessage;
            this._reRenderEntireTree(this._state);
        }
    }
}

export const addPostActionCreator = () => ({ 
    type: ADD_POST 
});

export let updateNewPostTextActionCreator = (text) => ({
    type: UPDATE_NEW_POST_TEXT, 
    updatedText: text,
});

export const sendMessageActionCreator = () => ({ 
    type: SEND_MESSAGE
});

export let updateNewMessageTextActionCreator = (text) => ({
    type: UPDATE_NEW_MESSAGE_TEXT, 
    updatedMessage: text,
});

export default store;
window.store = store;